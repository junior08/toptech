package com.example.toptech.exception;

import com.example.toptech.entities.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;

import static java.time.LocalDateTime.now;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = {ApiRequestException.class})
    public ResponseEntity<Object> handlerApiRequestException(ApiRequestException e){
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
      ApiExceptionData apiExceptionData =  new ApiExceptionData(
              e.getMessage(),
              e,
              badRequest,
              ZonedDateTime.now(ZoneId.of("Z"))
        );
      return new ResponseEntity<>(apiExceptionData, badRequest);
    }
}
