package com.example.toptech.utils;

import org.apache.coyote.Request;
import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;

import javax.validation.ClockProvider;
import javax.validation.ConstraintValidatorContext;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import java.nio.charset.MalformedInputException;
import java.util.stream.Collectors;

public class ValidationUtils {

    private String locationValues;
    boolean isValidURl (String s) throws MalformedInputException, URISyntaxException{
        try {
             new URI(s).toURL();
             return  true;
        }catch (URISyntaxException | MalformedURLException e){
            return false;
        }
    }
    boolean isValidUrlTwo(String url) throws MalformedInputException{
        URLValidator validator = new URLValidator( );
        ConstraintValidatorContext context = new ConstraintValidatorContext() {
            @Override
            public void disableDefaultConstraintViolation() {

            }

            @Override
            public String getDefaultConstraintMessageTemplate() {
                return null;
            }

            @Override
            public ClockProvider getClockProvider() {
                return null;
            }

            @Override
            public ConstraintViolationBuilder buildConstraintViolationWithTemplate(String s) {
                return null;
            }

            @Override
            public <T> T unwrap(Class<T> aClass) {
                return null;
            }
        };
        return validator.isValid( url, context);
    }

    public  synchronized  void serverRequest(Request request) {
       /*
        if (lastServedId >= SERVERS.size()) {
            lastServedId = 0;
        }
            Server server = SERVERS.get(lastServedId++);
            server.serve(request);
       * */
    }

}
