package com.example.toptech.services;

import com.example.toptech.dto.RequestTransaction;
import com.example.toptech.dto.ResponseTransaction;

public interface TransactionService {
    ResponseTransaction doTransaction(RequestTransaction txn);
}
