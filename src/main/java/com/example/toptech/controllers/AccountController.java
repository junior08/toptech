package com.example.toptech.controllers;


import com.example.toptech.dto.AccountDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.ResponseData;
import com.example.toptech.exception.ApiRequestException;
import com.example.toptech.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static java.time.LocalDateTime.now;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
    Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<ResponseData> create(@RequestBody Account account){
        logger.info("Initialization request");
        return ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("Create Account")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("account", accountService.created(account)  ))
                        .build()
        );

    }
    @PostMapping("/save-dto")
    public ResponseEntity<ResponseData> save(@RequestBody Account account){
        logger.info("Initialization request");
        return ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("Create Account with the DTO")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("account", accountService.save(account)  ))
                        .build()
        );

    }

    @GetMapping
    public ResponseEntity<ResponseData> list(){
        return ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("list Account")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("accounts", accountService.list()  ))
                        .build()
        );
    }
    @GetMapping("/{id}")
    public ResponseEntity<ResponseData> getOneAccount(@RequestParam Long id){
        return ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("Create Account")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("account", accountService.getOneAccount(id)  ))
                        .build()
        );
    }

    @GetMapping("/cuurent-amount/{id}")
    public ResponseEntity<ResponseData> getCurrentAmount(@RequestParam Long id){
        return ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("Get Current Amount for One Account")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("Current Amount", accountService.getAmount(id)))
                        .build()
        );
    }

}
