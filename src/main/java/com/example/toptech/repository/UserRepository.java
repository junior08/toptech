package com.example.toptech.repository;

import com.example.toptech.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<User, Long> {
    User findByUsername( final String username);
}
