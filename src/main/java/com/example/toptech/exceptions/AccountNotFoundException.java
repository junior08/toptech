package com.example.toptech.exceptions;

public class AccountNotFoundException extends  Exception{

    public AccountNotFoundException (String s){
        super(s);
    }
}
