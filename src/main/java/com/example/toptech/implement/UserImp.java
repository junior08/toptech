package com.example.toptech.implement;

import com.example.toptech.entities.Role;
import com.example.toptech.entities.User;
import com.example.toptech.repository.RoleRepository;
import com.example.toptech.repository.UserRepository;
import com.example.toptech.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class UserImp implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Override
    public User createUser(User user) {
        log.info("Saving a new user in the database");
        return userRepository.save(user);
    }

    @Override
    public User getUser(Long id) {
        log.info("Getting user in the database");
        return userRepository.findById(id).get();
    }

    @Override
    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String username, String name) {
        try {
            User user = userRepository.findByUsername(username);
            Role role = roleRepository.findByName(name);
            log.info(" get the role {} in the database", role.getName());
            user.getRoles().add(role);
            //user.setRoles(Arrays.asList(role));
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void deleteRoleToUser(String username, String name) {

    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public Role getRoleByName(String name) {
        return  roleRepository.findByName(name);
    }
}
