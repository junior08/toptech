package com.example.toptech.batch;

import com.example.toptech.entities.Customer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component

public class CustomerBatchItemProcessor implements ItemProcessor<Customer, Customer> {

    @Override
    public Customer process(Customer customer) throws Exception {
        customer.setAccounts(null);
        return customer;
    }
}
