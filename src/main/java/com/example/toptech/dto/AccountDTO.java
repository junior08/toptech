package com.example.toptech.dto;

import com.example.toptech.entities.Account;
import com.example.toptech.entities.Customer;
import com.example.toptech.repository.CustomerRepository;
import lombok.Getter;
import lombok.Setter;



public class AccountDTO {
    private CustomerRepository customerRepository;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumberAccount() {
        return numberAccount;
    }

    public void setNumberAccount(String numberAccount) {
        this.numberAccount = numberAccount;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    private  Long id;
    private  String numberAccount;

    private  Long customerId;



    public  AccountDTO(Account account){
      this.id =  account.getId();
      this.numberAccount = account.getNumberAccount();
      this.customerId    = account.getCustomer().getId();
    }


    public  Account toEntity(){
        Account account = new Account();
       if(this.id != null) {
           account.setId(this.id);
       }
        account.setNumberAccount(this.numberAccount);
        account.setCustomer(customerRepository.findById(this.customerId).get());
        return  account;
    }
}
