package com.example.toptech.dto;

import com.example.toptech.entities.TypeOperation;
import com.example.toptech.repository.AccountRepository;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Enumerated;

@Getter
@Setter
public class OperationSavingDTO {
    private Long creditId;
    private Long debitId;
    private String txnCode;
    private int amount;
    @Enumerated
    private TypeOperation typeOpeartion;
    private String message;
    private Boolean passed;


}
