package com.example.toptech.entities;

public enum TypeTransaction {
    DEPOT,
    RETRAIT,
    TRANSFERT
}
