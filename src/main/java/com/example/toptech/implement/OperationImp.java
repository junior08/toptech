package com.example.toptech.implement;

import com.example.toptech.dto.AccountDTO;
import com.example.toptech.dto.OperationDTO;
import com.example.toptech.dto.OperationSavingDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.Compte;
import com.example.toptech.entities.Operation;
import com.example.toptech.exceptions.AccountNotFoundException;
import com.example.toptech.repository.AccountRepository;
import com.example.toptech.repository.OperationRepository;
import com.example.toptech.services.OperationService;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class OperationImp implements OperationService {

    private  final OperationRepository operationRepository;
    private  final AccountRepository accountRepository;

    public OperationImp(OperationRepository operationRepository, AccountRepository accountRepository) {
        this.operationRepository = operationRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Operation created(Operation operation) {
        // Un get the Account debit
        //
        return null;
    }

    @Override
    public Collection<Operation> list(int limit) {
        return null;
    }

    @Override
    public Operation get(Long id) {
        return null;
    }

    @Override
    public Operation update(Operation operation) {
        return null;
    }

    @Override
    public Boolean deleted(Long id) {
        return null;
    }

    @Override
    public OperationDTO createEntity(OperationSavingDTO dto) throws AccountNotFoundException {

        // get the account Debit
        // verify if the amount available is more than the amount
        // if the operation is OK

        Operation operation = new Operation();
        operation.setAmount(dto.getAmount());
        operation.setTypeOperation(dto.getTypeOpeartion());
        operation.setTxnCode("FT0000016");
        operation.setDebit(accountRepository.findById(dto.getCreditId()).get());
        operation.setCredit(accountRepository.findById(dto.getCreditId()).get());
        operation.setTxnCode(dto.getTxnCode());
        operationRepository.save(operation);
        return new OperationDTO(operation);
    }

    @Override
    public Collection<Operation> paginatedListOperation(int limit, int size) {
        PageRequest pageable =  PageRequest.of(size, limit);
        return operationRepository.findAll(pageable).toList();
    }

    @Override
    public OperationDTO retrait(Compte debit, Compte credit, int amount) {
       try {
           if (amount == 0) return null;
       }
       catch (Exception e){
         return null;
       }
        return null;
    }

    @Override
    public OperationDTO verser(Compte dto, int amount) {
        return null;
    }

    @Override
    public OperationDTO retrait(Compte dto, int amount) {
        return null;
    }
}
