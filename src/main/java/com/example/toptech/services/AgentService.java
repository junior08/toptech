package com.example.toptech.services;

import com.example.toptech.dto.SaveAgentDTO;
import com.example.toptech.entities.Agent;

import java.util.Collection;

public interface AgentService {
    Agent save(SaveAgentDTO agent);
    Agent getOne(Long id);
    void delete(Long id);
    Collection<Agent> list();
}
