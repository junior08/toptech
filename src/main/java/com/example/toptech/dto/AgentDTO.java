package com.example.toptech.dto;

import com.example.toptech.entities.Agent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;

@Getter
@Setter
public class AgentDTO {
    private  Long id;
    private String firstname;
    private String lastname;
    private String password;
    private int level;
    private String address;
    private String cni;

    public AgentDTO(Agent agent){
        this.id        = agent.getId();
        this.address   = agent.getAddress();
        this.cni       = agent.getCni();
        this.level     = agent.getLevel();
        this.password  = agent.getPassword();
        this.firstname = agent.getFirstname();
        this.lastname  = agent.getLastname();
    }

}
