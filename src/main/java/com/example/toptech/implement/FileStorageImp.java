package com.example.toptech.implement;

import com.example.toptech.services.FileStorageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class FileStorageImp implements FileStorageService {

    private final Path rootLocation;

    @Autowired
    public FileStorageImp(){
        /*
        *  Initialize
        * */
        this.rootLocation = Paths.get("./storage");
    }
    @Override
    @PostConstruct
    public void init() {
        try {
            /*
             *  Create a directory with name storage
             * StringUtils.cleanPath
             * */
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String storeFile(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();
        try(InputStream inputStream = file.getInputStream()){
            assert filename != null;
            Files.copy(inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        return filename;
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation)).map(this.rootLocation::relativize);
        } catch (IOException e) {	return null; }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        Path file = load(filename);
        Resource resource = null;
        try {
            resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                System.out.println("File not found " + filename);
            }
        } catch (MalformedURLException e) { }
        return resource;
    }

    @Override
    public Boolean deleteAll(String name) {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
        return true;
    }
}
