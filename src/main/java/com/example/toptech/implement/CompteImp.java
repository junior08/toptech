package com.example.toptech.implement;

import com.example.toptech.dto.CompteDTO;
import com.example.toptech.repository.CompteRepository;
import com.example.toptech.services.CompteService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;


@Service
@Transactional
public class CompteImp implements CompteService {

    private final CompteRepository compteRepository;

    public CompteImp(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }


    @Override
    public CompteDTO create(CompteDTO dto) {
        return null;
    }

    @Override
    public Collection<CompteDTO> list() {
        return null;
    }

    @Override
    public CompteDTO getOneCompte(Long id) {
        return null;
    }

    @Override
    public Collection<CompteDTO> getCompteByType(String type) {
        return null;
    }

    @Override
    public boolean deleteCompte(Long id) {
        return false;
    }

    @Override
    public CompteDTO update(CompteDTO compteDTO) {
        return null;
    }
}
