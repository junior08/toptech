package com.example.toptech.implement;

import com.example.toptech.dto.SaveAgentDTO;
import com.example.toptech.entities.Agent;
import com.example.toptech.repository.AgentRepository;
import com.example.toptech.services.AgentService;
import com.example.toptech.services.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class AgentImp implements AgentService {
    private final AgentRepository agentRepository;
    private final FileStorageService fileStorageService;
    public  AgentImp(AgentRepository agentRepository, FileStorageService fileStorageService){
        this.agentRepository = agentRepository;
        this.fileStorageService = fileStorageService;
    }


    @Override
    public Agent save(SaveAgentDTO agent) {
        Agent agent1 = new Agent();
        MultipartFile file = agent.getFile();
        String filename = file.getOriginalFilename();

        return null;
    }

    @Override
    public Agent getOne(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Collection<Agent> list() {
        return null;
    }
}
