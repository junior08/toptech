package com.example.toptech.services;

import com.example.toptech.dto.AccountDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.Operation;

import java.util.Collection;

public interface AccountService {
    Account created(Account account);
    AccountDTO save(Account dto);
    Collection<AccountDTO> list ();
    Account getOneAccount(Long id);
    Account update(Account account);
    Boolean deleted(Long id);
    Collection<Operation> listOperationByUser(Long id);
    int getSoldeAccount(Account account);
    int getAmount(Long id);


}
