package com.example.toptech.entities;


import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 255)
    private String firstname;
    @Column(nullable = false, length = 255)
    private String lastname;
    @Column(nullable = false, length = 255)
    private String adresse;
    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private Collection<Account> accounts;
}
