package com.example.toptech.services;

import com.example.toptech.dto.CompteDTO;

import java.util.Collection;

public interface CompteService {

    CompteDTO create(CompteDTO dto);
    Collection<CompteDTO> list();
    CompteDTO getOneCompte(Long id);
    Collection<CompteDTO> getCompteByType(String type);
    boolean deleteCompte(Long id);
    CompteDTO update(CompteDTO compteDTO);

}
