package com.example.toptech.implement;

import com.example.toptech.dto.AccountDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.Operation;
import com.example.toptech.exception.ApiRequestException;
import com.example.toptech.repository.AccountRepository;
import com.example.toptech.services.AccountService;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class AccountImp implements AccountService {
    private final AccountRepository accountRepository;
    @Override
    public Account created(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public AccountDTO save(Account account) {
        account = accountRepository.save(account);
        return new AccountDTO(account);
    }


    public AccountImp(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Collection<AccountDTO> list() {
        Collection<Account> list = accountRepository.findAll();
        Collection<AccountDTO> accountDTOS = new ArrayList<>();
        for (Account a : list){
            AccountDTO dto = new AccountDTO(a);
            accountDTOS.add(dto);
        }
        return accountDTOS;
    }

    @Override
    public Account getOneAccount(Long id)  {
        return accountRepository.findById(id).get();
    }

    @Override
    public Account update(Account account) {
        return null;
    }

    @Override
    public Boolean deleted(Long id) {
        return null;
    }

    @Override
    public Collection<Operation> listOperationByUser(Long id) {
        // Collection<Operation> operations = accountRepository.
        return null;
    }

    @Override
    public int getSoldeAccount(Account account) {
        AtomicInteger credit = new AtomicInteger();
        AtomicInteger debit = new AtomicInteger();
        if (account.getId()!= null){
            Collection<Operation> operations = account.getOperations();
            operations.stream()
                    .filter( operation -> {
                        if (Objects.equals(operation.getCredit().getId(), account.getId())) {
                            credit.addAndGet(operation.getAmount());
                        }
                        return true;
                    });
            operations.stream()
                    .filter( operation -> {
                        if (Objects.equals(operation.getCredit().getId(), account.getId())) {
                            debit.addAndGet(operation.getAmount());
                        }
                        return true;
                    });
        }
        return credit.get() - debit.get();
    }

    @Override

    public int getAmount(Long id) {
        if(accountRepository.findById(id).isEmpty()){
            throw new ApiRequestException("Cannot find Account with this ID: " + id);
        }
        return accountRepository.getInfosByAccountCredit(id) - accountRepository.getInfosByAccountDebit(id);
    }
}
