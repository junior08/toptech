package com.example.toptech.exception;

import org.springframework.http.ResponseEntity;

public class ApiRequestException  extends RuntimeException{

    public  ApiRequestException(String message){
        super(message);
    }
    public ApiRequestException(String message, Throwable cause){
        super(message, cause);
    }


}
