package com.example.toptech;

import com.example.toptech.entities.Role;
import com.example.toptech.entities.User;
import com.example.toptech.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
public class ToptechApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToptechApplication.class, args);
	}

	/*
	@Bean
	CommandLineRunner run(UserService userService){
		return args -> {
			String roleName = "ROLE_AMETH";
			Role role = userService.saveRole(new Role(null, roleName));
			Long id = Long.valueOf(2);
			User user = userService.getUser(id);
			System.out.println("------------------ le role est ajouté  ----------------");
			user.getRoles().add(role);
			System.out.println("------------------ get role ----------------  "+ user.getRoles());
			userService.createUser(user);

		};
	}
*
	 */

}
