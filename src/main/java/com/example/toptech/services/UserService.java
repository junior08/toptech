package com.example.toptech.services;

import com.example.toptech.entities.Role;
import com.example.toptech.entities.User;

import java.util.List;

public interface UserService {

    User createUser(User user);
    User getUser(Long id);
    Role saveRole(Role role);
    void addRoleToUser(String username, String name);
    void deleteRoleToUser(String username, String name);
    List<User> getUsers();

    Role getRoleByName(String name);
}
