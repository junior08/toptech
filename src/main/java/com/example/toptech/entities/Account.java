package com.example.toptech.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import java.util.Collection;



@Entity

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    @ManyToOne
    @JoinColumn(name="customer_id", nullable=false)
    private  Customer customer;
    private  String numberAccount;
    @OneToMany(mappedBy = "credit")
    private  Collection<Operation> operations;
    @OneToMany(mappedBy = "debit")
    private  Collection<Operation> operationsDebitted;
    @OneToMany(mappedBy = "account")
    private Collection<Transaction> transactions;

}
