package com.example.toptech.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class DepotTxnDTO {

    private Long id;
    private Long baseAccountId;
    private Long accountId;
    private int amountTxn;

}
