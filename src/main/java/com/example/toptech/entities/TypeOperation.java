package com.example.toptech.entities;

import javax.persistence.Entity;

public enum TypeOperation {
    DEPOT,
    RETRAIT,
    TRANSFERT
}
