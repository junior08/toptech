package com.example.toptech.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseTransaction {
    protected LocalDateTime timeStamp;
    protected  Long account1;
    protected  Long account2;
    private Boolean isValid;
    private int amount;
    private  String message;
}
