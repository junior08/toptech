package com.example.toptech.controllers;


import com.example.toptech.dto.RequestCustomerSaveDTO;
import com.example.toptech.entities.ResponseData;
import com.example.toptech.exception.ApiRequestException;
import com.example.toptech.repository.CustomerRepository;
import com.example.toptech.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import static java.time.LocalDateTime.now;
@RestController
@RequestMapping(path = "/api/customers")
public class CustomerController {
    Logger logger = LoggerFactory.getLogger(CustomerController.class);
    private final CustomerService customerService;
    private final CustomerRepository customerRepository;

    public CustomerController(CustomerService customerService,
                              CustomerRepository customerRepository) {
        this.customerService = customerService;
        this.customerRepository = customerRepository;
    }
    @PostMapping
    @CrossOrigin
    public ResponseEntity<ResponseData> create(@RequestBody RequestCustomerSaveDTO customer){
        logger.info("We Start the action");
        return  ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("Messages Sending")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("users", customerService.created(customer) ))
                        .build()
        );
    }
    @GetMapping
    @CrossOrigin
    public  ResponseEntity<ResponseData> list() throws ApiRequestException {
        return  ResponseEntity.ok(
                ResponseData.builder()
                        .timeStamp(now())
                        .message("Messages Sending")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .data(Map.of("customers", customerService.list() ))
                        .build()
        );
    }
    @GetMapping("/{id}")
    @CrossOrigin
    public   ResponseEntity<ResponseData> getCustomer(@RequestParam Long id){
        logger.info("get Customer By ID: {} ", id);
        return ResponseEntity.ok(
               ResponseData.builder()
                       .timeStamp(now())
                       .message("Get One Customer")
                       .status(HttpStatus.OK)
                       .statusCode(HttpStatus.OK.value())
                       .data(Map.of("customer", customerService.get(id)))
                       .build()
        );
    }
}
