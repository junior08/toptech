package com.example.toptech.dto;


import com.example.toptech.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleDTO {

    private Long id;
    private String name;

    public  RoleDTO(Role role){
        this.id = role.getId();
        this.name = role.getName();
    }
    public Role EntityToRole(){
        Role role  = new Role();
        role.setId(this.id);
        role.setName(this.name);
        return  role;
    }
}
