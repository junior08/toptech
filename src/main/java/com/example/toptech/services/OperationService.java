package com.example.toptech.services;

import com.example.toptech.dto.AccountDTO;
import com.example.toptech.dto.OperationDTO;
import com.example.toptech.dto.OperationSavingDTO;
import com.example.toptech.entities.Compte;
import com.example.toptech.entities.Operation;
import com.example.toptech.exceptions.AccountNotFoundException;

import java.util.Collection;

public interface OperationService {
    Operation created(Operation operation);
    Collection<Operation> list (int limit);
    Operation get(Long id);
    Operation update(Operation operation);
    Boolean deleted(Long id);
    OperationDTO createEntity(OperationSavingDTO dto) throws AccountNotFoundException;
    Collection<Operation> paginatedListOperation(int limit, int size);
    OperationDTO retrait(Compte debit, Compte credit, int amount);
    OperationDTO verser(Compte dto, int amount);
    OperationDTO retrait(Compte dto, int amount);




}
