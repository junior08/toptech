package com.example.toptech.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransferTxnDTO {
    private Long id;
    private Long accountIdDebit;
    private Long accountIdCredit;
    private int amountTxn;
}
