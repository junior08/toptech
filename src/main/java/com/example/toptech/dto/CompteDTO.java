package com.example.toptech.dto;

import com.example.toptech.entities.Compte;
import com.example.toptech.entities.CompteCourant;
import com.example.toptech.entities.CompteEpargne;
import com.example.toptech.entities.StatusCompte;
import com.example.toptech.repository.CompteRepository;
import com.example.toptech.repository.CustomerRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompteDTO {

    private CustomerRepository customerRepository;
    private Long id;
    private String numereCompte;
    private CustomerDTO customer;
    private StatusCompte statusCompte;
    private String type;


    public CompteDTO(CompteEpargne compte){
        this.id = compte.getId();

        this.numereCompte = compte.getNumereCompte();
        this.statusCompte = compte.getStatusCompte();
        this.customer  = new CustomerDTO(compte.getCustomer());
        this.type = "CE";

    }
    public CompteDTO(CompteCourant compte){
        this.id = compte.getId();

        this.numereCompte = compte.getNumereCompte();
        this.statusCompte = compte.getStatusCompte();
        this.customer  = new CustomerDTO(compte.getCustomer());
        this.type = "CC";

    }
    public CompteEpargne toEntityEpargne(){
        CompteEpargne compteEpargne = new CompteEpargne();
        compteEpargne.setId(this.id);
        compteEpargne.setNumereCompte(this.numereCompte);
        compteEpargne.setStatusCompte(this.statusCompte);
        compteEpargne.setCustomer(customerRepository.findById(this.getCustomer().getId()).get());
        return  compteEpargne;
    }
    public  CompteCourant toEntityCourant(){
        CompteCourant compteCourant = new CompteCourant();
        compteCourant.setId(this.id);
        compteCourant.setNumereCompte(this.numereCompte);
        compteCourant.setStatusCompte(this.statusCompte);
        compteCourant.setCustomer(customerRepository.findById(this.getCustomer().getId()).get());
        return  compteCourant;
    }
}
