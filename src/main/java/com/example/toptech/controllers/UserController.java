package com.example.toptech.controllers;


import com.example.toptech.entities.Role;
import com.example.toptech.entities.User;
import com.example.toptech.repository.RoleRepository;
import com.example.toptech.services.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
@Slf4j
public class UserController {

    private final UserService userService;
    private  final RoleRepository roleRepository;

    @GetMapping("/list")
    public ResponseEntity<List<User>> getUsers(){
        return  ResponseEntity.ok().body(userService.getUsers());
    }

    @PostMapping("/save")
    public ResponseEntity<User> create(@RequestBody User user){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/user/save").toUriString());
        return  ResponseEntity.ok().body(userService.createUser(user));
    }

    @PostMapping("/add/role")
    public ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserForm form){
        userService.addRoleToUser(form.getUsername(), form.getRolename());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/add")
    public ResponseEntity<Role> addRoleT(@RequestBody RoleToUserForm form){
           // userService.addRoleToUser(form.getUsername(), form.getRolename());
        System.out.println("Save role for user");
        Role role = roleRepository.findByName(form.getRolename());
        log.info("Get one role on the database {} ", role);
            return ResponseEntity.ok().body(role);
    }
}

@Data
class RoleToUserForm{
    private  String username;
    private  String rolename;
}
