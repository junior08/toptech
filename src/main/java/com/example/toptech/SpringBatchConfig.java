
package com.example.toptech;


import com.example.toptech.entities.Customer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Configuration
@EnableBatchProcessing
@Profile("spring")
@Service
public class SpringBatchConfig {


     private final JobBuilderFactory jobBuilderFactory;
     private final StepBuilderFactory stepBuilderFactory;
     private final ItemReader<Customer> customerItemReader;
     private final ItemWriter<Customer> customerItemWriter;
     private final ItemProcessor<Customer, Customer > customerItemProcessor;

    public SpringBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, ItemReader<Customer> customerItemReader, ItemWriter<Customer> customerItemWriter, ItemProcessor<Customer, Customer> customerItemProcessor) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.customerItemReader = customerItemReader;
        this.customerItemWriter = customerItemWriter;
        this.customerItemProcessor = customerItemProcessor;
    }

    @Bean
    public Job CustomerJob(){
        Step step = stepBuilderFactory.get("step-one-initializer")
                .<Customer, Customer>chunk(1000)
                .reader(customerItemReader)
                .processor(customerItemProcessor)
                .writer(customerItemWriter)
                .build();
        return jobBuilderFactory.get("data-start-load-job")
                .start(step)
                .build()
                ;
    }

    @Bean
    public FlatFileItemReader<Customer> itemReader(@Value("$inputFile") Resource resource ){
        FlatFileItemReader <Customer> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setName("MBAYE-KAY-FII");
        flatFileItemReader.setLinesToSkip(1); // Skip the first line of the resource
        flatFileItemReader.setResource(resource);
        flatFileItemReader.setLineMapper(lineMapper());
        return  flatFileItemReader;

    }

    @Bean
    public LineMapper<Customer> lineMapper(){
        DefaultLineMapper<Customer> lineMapper= new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("id", "firstname","lastname","adresse");
        lineMapper.setLineTokenizer(lineTokenizer);
        BeanWrapperFieldSetMapper<Customer> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Customer.class);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        return  lineMapper;
    }
}

