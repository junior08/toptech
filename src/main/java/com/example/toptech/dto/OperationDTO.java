package com.example.toptech.dto;

import com.example.toptech.entities.Account;
import com.example.toptech.entities.Operation;
import com.example.toptech.entities.TypeOperation;
import com.example.toptech.repository.AccountRepository;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Enumerated;
@Getter
@Setter
public class OperationDTO {
    private AccountRepository accountRepository;
    private Long id;
    private int amount;
    @Enumerated
    private TypeOperation typeOpeartion;
    private AccountDTO credit;
    private Long account_credit;
    private Long account_debit;
    private AccountDTO debit;
    private String txnCode;

    public  OperationDTO(Operation operation){
        this.id = operation.getId();
        this.amount = operation.getAmount();
        this.credit = new AccountDTO(operation.getCredit());
        this.debit= new AccountDTO(operation.getDebit());
        this.account_credit = this.credit.getId();
        this.account_debit  = this.debit.getId();
        this.typeOpeartion  = operation.getTypeOperation();
        this.txnCode = operation.getTxnCode();

    }
    public Operation toEntity(){
        Operation op = new Operation();
        op.setId(this.id);
        op.setAmount(this.amount);
        op.setTxnCode(this.txnCode);
        op.setTypeOperation(this.typeOpeartion);
        op.setDebit(accountRepository.findById(this.account_debit).get());
        op.setCredit(accountRepository.findById(this.account_credit).get());
        return  op;
    }


}
