package com.example.toptech.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestCustomerSaveDTO {
    private String firstname;
    private String lastname;
    private String adresse;
}
