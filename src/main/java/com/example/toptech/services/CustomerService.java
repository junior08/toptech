package com.example.toptech.services;

import com.example.toptech.dto.CustomerDTO;
import com.example.toptech.dto.RequestCustomerSaveDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.Customer;

import java.util.Collection;

public interface CustomerService {
    Customer created(RequestCustomerSaveDTO customer);
    Collection<CustomerDTO> list ();
    Customer get(Long id);
    Customer update(Long customerId, Customer customer);
    Boolean deleted(Long id);
    Collection<Account> listAccountByUser(Long id);
}
