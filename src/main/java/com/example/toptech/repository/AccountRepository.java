package com.example.toptech.repository;

import com.example.toptech.dto.InfoByAccountDTO;
import com.example.toptech.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface AccountRepository  extends JpaRepository<Account, Long> {

    @Query(value = "SELECT * FROM account a WHERE a.customer_id = ?1", nativeQuery = true)
    Collection<Account> getAccountByCustomer(Long customer_id);
    @Query(value = "SELECT SUM(ac.amount) FROM account, operation op  WHERE op.acc", nativeQuery = true)
    Collection<Account> getAccountByCustomerNative(Long customer_id);
    // Get Amount Debit in the Account
    @Query(value = "SELECT SUM(op.amount) FROM account a, operation op WHERE a.id =?1 and a.id = op.account1_id AND op.type_operation = \"RETRAIT\" GROUP BY a.id", nativeQuery = true)
    int getInfosByAccountDebit(Long id);

    @Query(value = "SELECT SUM(op.amount) FROM account a, operation op WHERE a.id =?1 and a.id = op.account2_id AND op.type_operation = \"DEPOT\" GROUP BY a.id", nativeQuery = true)
    int getInfosByAccountCredit(Long id);

}
