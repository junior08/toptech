package com.example.toptech.exception;



import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class ApiExceptionData {
    private final String message;
    private final  Throwable throwable;

    private final HttpStatus httpStatus;
    private final ZonedDateTime zonedDateTime;

    public ApiExceptionData(String message, Throwable throwable, HttpStatus httpStatus, ZonedDateTime zonedDateTime) {
        this.message = message;
        this.throwable = throwable;
        this.httpStatus = httpStatus;
        this.zonedDateTime = zonedDateTime;
    }


}
