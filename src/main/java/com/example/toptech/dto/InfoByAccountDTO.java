package com.example.toptech.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InfoByAccountDTO {

    private Long id;
    private Long customed_id;
    private int amount;
    private Long account1_id;
    private String txn_code;
}
