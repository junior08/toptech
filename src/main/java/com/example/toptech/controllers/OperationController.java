package com.example.toptech.controllers;


import com.example.toptech.dto.OperationDTO;
import com.example.toptech.dto.OperationSavingDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.DataResponse;
import com.example.toptech.entities.Operation;
import com.example.toptech.repository.AccountRepository;
import com.example.toptech.services.AccountService;
import com.example.toptech.services.OperationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping("/api/operation")
public class OperationController {
    Logger logger = LoggerFactory.getLogger(OperationController.class);

    private final OperationService operationService;
    private final AccountService accountService;

    public OperationController(OperationService operationService, AccountService accountService) {
        this.operationService = operationService;
        this.accountService = accountService;
    }

    @PostMapping
    public DataResponse create(@RequestBody OperationSavingDTO dto) throws Exception {
        DataResponse dataResponse = new DataResponse();
        // Check if the amount is available in the account debeting
        dataResponse.setTimeStamp(LocalDateTime.now());
            if(dto.getAmount() > accountService.getAmount(dto.getDebitId())){
                dataResponse.setData(Map.of("data", "null"));
                dataResponse.setMessage("Transaction a échoué: montant insuffisant");
                logger.info("La transaction a échoué");
            }
            else {
                accountService.getAmount(dto.getCreditId());
                logger.info("Initialisation de operation");
                dataResponse.setDeveloperMessage("Creation d'un object operation: Transaction");
                dataResponse.setMessage("Transaction a réussi");
                dataResponse.setData(Map.of("data", operationService.createEntity(dto)));
            }
            dataResponse.setStatus(HttpStatus.OK);
            dataResponse.setStatusCode(HttpStatus.OK.value());
        return  dataResponse;
    }
    @GetMapping("/list/{size}/{limite}")
    public DataResponse paginatedDataOperation(@PathVariable int size, @PathVariable int limite) throws Exception{
        DataResponse dataResponse = new DataResponse();
            logger.info("List des operations Paginated");
            dataResponse.setTimeStamp(LocalDateTime.now());
            dataResponse.setDeveloperMessage("Pagination Operation");
            Collection<Operation> list = operationService.paginatedListOperation(size, limite);
            dataResponse.setData(Map.of("data", list));
            System.out.printf("les données" + list);
            dataResponse.setStatus(HttpStatus.OK);
            dataResponse.setStatusCode(HttpStatus.OK.value());
            dataResponse.setMessage("List des operations");
        return dataResponse;
    }
}
