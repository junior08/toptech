package com.example.toptech.dto;

import com.example.toptech.entities.Account;
import com.example.toptech.entities.Customer;
import com.example.toptech.repository.AccountRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomerDTO {
    private AccountRepository accountRepository;
    private  Long id;
    private String firstname;
    private String lastname;
    private List<AccountDTO> accounts;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public List<AccountDTO> getAccountDTOList() {
        return accounts;
    }

    public void setAccountDTOList(List<AccountDTO> accountDTOList) {
        this.accounts = accountDTOList;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public  CustomerDTO(Customer customer){
        this.id = customer.getId();
        this.firstname = customer.getFirstname();
        this.lastname  = customer.getLastname();
        Collection<Account> accounts = customer.getAccounts();
        Collection<AccountDTO> accountsAccountDTOS = new ArrayList<>();
        accounts.forEach(account -> {
            AccountDTO dto = new AccountDTO(account);
            accountsAccountDTOS.add(dto);
        });
        this.accounts = (List<AccountDTO>) accountsAccountDTOS;
    }

    private Customer toEntity(){
        Customer c = new Customer();
        c.setId(this.id);
        c.setFirstname(this.firstname);
        c.setLastname(this.lastname);
        c.setAccounts(accountRepository.getAccountByCustomer(this.id));
        return  c;
    }
}
