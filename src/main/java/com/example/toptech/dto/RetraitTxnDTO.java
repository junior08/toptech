package com.example.toptech.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetraitTxnDTO {
    private Long id;
    private Long baseAccountId;
    private Long accountId;
    private int amountTxn;
}
