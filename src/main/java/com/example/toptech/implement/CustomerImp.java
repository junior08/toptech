package com.example.toptech.implement;

import com.example.toptech.dto.CustomerDTO;
import com.example.toptech.dto.RequestCustomerSaveDTO;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.Customer;
import com.example.toptech.repository.CustomerRepository;
import com.example.toptech.services.CustomerService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;


@Service
@Transactional

public class CustomerImp implements CustomerService {

    private CustomerRepository customerRepository;

    public CustomerImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer created(RequestCustomerSaveDTO customer) {
        Customer customer1 = Customer.builder()
                .firstname(customer.getFirstname())
                .lastname(customer.getLastname())
                .adresse(customer.getAdresse())
                .build();
        return customerRepository.save(customer1);
    }

    @Override
    public Collection<CustomerDTO> list() {
        Collection<Customer> customers = customerRepository.findAll();
        Collection<CustomerDTO> dtos = new ArrayList<>();
        customers.forEach(customer -> {
                CustomerDTO dto = new CustomerDTO(customer);
                dtos.add(dto);
        });
        return dtos;
    }

    @Override
    public Customer get(Long id) {
        return customerRepository.findById(id).get();
    }

    @Override
    public Customer update(Long customerId, Customer customer) {
        Customer c = customerRepository.findById(customerId).get();
        if(c.getId() != null) {
          return null;
        }
        return customerRepository.save(customer);
    }

    @Override
    public Boolean deleted(Long id) {
        Customer c = customerRepository.findById(id).get();
        if(c.getId() != null) {
            customerRepository.deleteById(id);
            return  true;
        }
        return false;
    }

    @Override
    public Collection<Account> listAccountByUser(Long id) {
        return null;
    }
}
