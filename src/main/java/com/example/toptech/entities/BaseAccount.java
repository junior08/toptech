package com.example.toptech.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BaseAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    private  String codeAccount;
    private  int baseAmount;
    @OneToMany(mappedBy = "baseAccount")
    private Collection<Operation> operations;
}
