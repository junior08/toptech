package com.example.toptech.implement;

import com.example.toptech.dto.RequestTransaction;
import com.example.toptech.dto.ResponseTransaction;
import com.example.toptech.entities.Account;
import com.example.toptech.entities.Transaction;
import com.example.toptech.entities.TypeTransaction;
import com.example.toptech.repository.AccountRepository;
import com.example.toptech.repository.TransactionRepository;
import com.example.toptech.services.TransactionService;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class RetraitTxnImp implements TransactionService {
    private  final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    public RetraitTxnImp(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }
    /**
     * this is a WITHDRAWAL Transaction
     * We have 2 accounts : Account 1 is a sender
     * Account 2 is a receiver
     * @param txn  : RequestTransaction
     * @return ResponseTransaction
     */
    @Override
    public ResponseTransaction doTransaction(RequestTransaction txn) {

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        ResponseTransaction responseTransaction = new ResponseTransaction();
        responseTransaction.setAmount(txn.getAmount());
        responseTransaction.setAccount1(txn.getAccountid());
        responseTransaction.setAccount2(txn.getAccountid1());
        Account ac1 =  accountRepository.findById(txn.getAccountid()).orElseThrow();
        Account ac2 =  accountRepository.findById(txn.getAccountid1()).orElseThrow();
        Date date = new Date();
        try {
            Transaction  transaction = Transaction.builder()
                    .amountTransaction(txn.getAmount())
                    .account(ac1)
                    .accountTransaction(ac2)
                    .typeTransaction(TypeTransaction.RETRAIT)
                    .createdAt(Timestamp.valueOf(sdf2.format(date)))
                    .updateAt(Timestamp.valueOf(sdf2.format(date)))
                    .build();
          Transaction tx =  transactionRepository.saveAndFlush(transaction);
          if(tx.getId() != null){
              responseTransaction.setMessage("Transaction is success");
              responseTransaction.setIsValid(true);
          }
        }catch (Exception e){
            responseTransaction.setMessage("Error: " + String.valueOf(e));
            responseTransaction.setIsValid(false);
        }
        return responseTransaction;
    }
}
