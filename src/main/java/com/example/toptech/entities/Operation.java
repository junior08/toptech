package com.example.toptech.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    @Enumerated(EnumType.STRING)
    private TypeOperation typeOperation;
    private int amount;
    @ManyToOne
    @JoinColumn(name="account1_id", insertable = true, updatable = false)
    @JsonIgnore
    private Account credit;
    @ManyToOne
    @JoinColumn(name="account2_id", insertable = false, updatable = false , nullable = true)
    @JsonIgnore
    private Account debit;
    @Column(unique = true, length = 10, nullable = false)
    private String txnCode;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "base_account_id", nullable = true)
    private BaseAccount baseAccount;

}
