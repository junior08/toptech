package com.example.toptech;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ToptechApplicationTests {

	@Test
	void contextLoads() {
		String text = "Khazim";
		String contentText = "Kh";
		Assert.assertTrue(text.contains(contentText));
	}

}
