package com.example.toptech.repository;

import com.example.toptech.entities.Operation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {

    @Query(value = "SELECT * FROM operation WHERE amount != null", nativeQuery = true)
    Page<Operation> findAllByAmount(Pageable pageable);
}
