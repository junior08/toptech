package com.example.toptech.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class SaveAgentDTO {

    private AgentDTO agentDTO;
    private MultipartFile file;
}
