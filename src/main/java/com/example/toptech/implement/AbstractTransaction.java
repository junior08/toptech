package com.example.toptech.implement;

import com.example.toptech.dto.RequestTransaction;
import com.example.toptech.dto.ResponseTransaction;
import com.example.toptech.services.TransactionService;

public abstract class AbstractTransaction {
    public abstract ResponseTransaction createTransaction(RequestTransaction txn);
}
