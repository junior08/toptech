package com.example.toptech.repository;

import org.springframework.batch.core.repository.JobRepository;

public interface CustomerJobRepository extends JobRepository {
}
