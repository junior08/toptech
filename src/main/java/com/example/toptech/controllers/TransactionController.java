package com.example.toptech.controllers;


import com.example.toptech.dto.RequestTransaction;
import com.example.toptech.dto.ResponseTransaction;
import com.example.toptech.entities.DataResponse;
import com.example.toptech.implement.DepotTxnImp;
import com.example.toptech.implement.RetraitTxnImp;
import com.example.toptech.implement.TransaferTxnImp;
import com.example.toptech.repository.AccountRepository;
import com.example.toptech.repository.TransactionRepository;
import com.example.toptech.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Map;


@RestController
@Slf4j
@RequestMapping("/api/transactions")
public class TransactionController {

    Logger logger = LoggerFactory.getLogger(TransactionController.class);

    private  TransactionService transactionService;
    public ResponseTransaction response;
    public TransactionRepository transactionRepository;
    public AccountRepository accountRepository;
    public DataResponse create(@RequestBody RequestTransaction request){
        DataResponse dataResponse = new DataResponse();
        // Check if the amount is available in the account debeting
        dataResponse.setTimeStamp(LocalDateTime.now());
        if(request.getAmount() <= 0){
            dataResponse.setData(Map.of("data", "null"));
            dataResponse.setMessage("Transaction is failed");
            logger.info("Transaction is failed");
        }
        else {
            logger.info("Initialization transaction");
            dataResponse.setDeveloperMessage(" Transaction object is Created");
            dataResponse.setMessage("Transaction is succeed");
            //transactionService.doTransaction(transaction)
            if(request.getTypeTransaction().equals("DEPOT")){
                transactionService = new DepotTxnImp();
                response = transactionService.doTransaction(request);
            }
           else if(request.getTypeTransaction().equals("RETRAIT")){
                transactionService = new RetraitTxnImp(transactionRepository , accountRepository);
                response = transactionService.doTransaction(request);
            }
            else if(request.getTypeTransaction().equals("TRANSFER")){

                transactionService = new TransaferTxnImp();
                response = transactionService.doTransaction(request);
            }
            dataResponse.setData(Map.of("data", ""));
        }
        dataResponse.setStatus(HttpStatus.OK);
        dataResponse.setStatusCode(HttpStatus.OK.value());
        return  dataResponse;
    }
}
